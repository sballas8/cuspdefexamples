{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# $6_3$ knot rigidity computation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### $6_3$ is infinitesimally rigid rel. $\\partial M$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we show that the $6_3$ knot group is infinitesimally rigid rel. $\\partial$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $K$ be the $6_3$ knot. The fundamental group of $M=S^3\\backslash K$ is the group $\\Gamma=\\langle x,y\\mid xw=wy\\rangle$ where $w=yxy^{-1}x^{-1}y^{-1}xyx^{-1}y^{-1}x^{-1}yx$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In $PSL(2,\\mathbb{C})$ the holonomy, $\\rho_{hyp}$, of the complete hyperbolic structure on $M$ is given by \n",
    "\n",
    "$\\rho_{hyp}(x)= \\begin{pmatrix}1 & 1\\\\ 0 & 1\\end{pmatrix}$, $\\rho_{hyp}(y)=\\begin{pmatrix}1 & 0\\\\z&1\\end{pmatrix}$, where $z$ is a complex root of the irreducible polynomial $p(t)=1+t+2t^2+4t^3+5t^4+3t^5+t^6$ with positive imaginary part. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $z=u+iv$. We will need to perform several computation in the number field $Z=\\mathbb{Q}(u,v)$. \n",
    "\n",
    "We begin by generating this field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "t=polygen(QQ,'t')\n",
    "X.<z>=NumberField(1+t+2*t^2+4*t^3+5*t^4+3*t^5+t^6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$X$ contains 2 roots of $p(t)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[z, z^5 + 2*z^4 + 3*z^3 + 2*z^2 + z]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "z.galois_conjugates(X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$u=\\frac{z+\\overline{z}}{2}$ is contained in $Y$, but $v=\\frac{z-\\overline{z}}{2i}$ is not, so we need to adjoin $i$ to $Y$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "R.<t>=X[]\n",
    "Z.<i>=X.extension(t^2+1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now build $u$ and $v$ in $Z$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "gal_conjs=z.galois_conjugates(X);\n",
    "zz=gal_conjs[0]\n",
    "zzbar=gal_conjs[1]\n",
    "u=(zz+zzbar)/2\n",
    "v=(zz-zzbar)/(2*i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a sanity check we will verify that $z=u+iv$ has the right minimal polynomial"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "t^6 + 3*t^5 + 5*t^4 + 4*t^3 + 2*t^2 + t + 1"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(u+i*v).absolute_minpoly(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Lie algebra $\\mathfrak{g}$ splits as $\\mathfrak{g}\\cong \\mathfrak{so}(3,1)\\oplus \\mathfrak v$. \n",
    "\n",
    "We begin by defining a projection to $\\mathfrak{v}$\n",
    "\n",
    "If we use the form, $J$ listed in the paper we have to do computations over a slightly larger number field, but this can be avoided by changing the form to \n",
    "$$J=\\begin{pmatrix}\n",
    "0 & 0 & 0 & -1/2\\\\\n",
    "0 & 1 & 0 & 0\\\\\n",
    "0 & 0 & 1 & 0\\\\\n",
    "-1/2 & 0 & 0  & 0\n",
    "\\end{pmatrix}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def projv(a):\n",
    "    J=matrix(Z,[[0,0,0,-1/2],[0,1,0,0],[0,0,1,0],[-1/2,0,0,0]])\n",
    "    proj =1/2*(a+~J*(a.transpose())*J)\n",
    "    return proj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can embed $PSL(2,\\mathbb{C})$ in $SO(3,1)$. Under this embedding the above representation becomes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Xmat=matrix(Z,[[1,2,0,1],[0,1,0,1],[0,0,1,0],[0,0,0,1]])\n",
    "Xmati=~Xmat\n",
    "Ymat=matrix(Z,[[1,0,0,0],[u,1,0,0],[-v,0,1,0],[(u^2+v^2),2*u,-2*v,1]])\n",
    "Ymati=~Ymat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now check that the appropriate relation is satisfied"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0 0 0 0]\n",
       "[0 0 0 0]\n",
       "[0 0 0 0]\n",
       "[0 0 0 0]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "W=Ymat*Xmat*Ymati*Xmati*Ymati*Xmat*Ymat*Xmati*Ymati*Xmati*Ymat*Xmat\n",
    "Xmat*W-W*Ymat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we find a basis for the 9-dimensional vector space $\\mathfrak{v}$. \n",
    "\n",
    "$M$ is a $16\\times 9$ matrix whose columns are a basis of $\\mathfrak{v}$ written with respect to the standard basis (elementary matrices) on $\\mathfrak{gl}(4,\\mathbb{R})$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(16, 9)\n",
      "9\n"
     ]
    }
   ],
   "source": [
    "v1=diagonal_matrix([-1,1,0,0]);\n",
    "v2=diagonal_matrix([-1,0,1,0]);\n",
    "v3=matrix([[0,1,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]);\n",
    "v4=matrix([[0,0,1,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]);\n",
    "v5=matrix([[0,0,0,1],[0,0,0,0],[0,0,0,0],[0,0,0,0]]);\n",
    "v6=matrix([[0,0,0,0],[0,0,1,0],[0,0,0,0],[0,0,0,0]]);\n",
    "v7=matrix([[0,0,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0]]);\n",
    "v8=matrix([[0,0,0,0],[0,0,0,0],[1,0,0,0],[0,0,0,0]]);\n",
    "v9=matrix([[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,0,0,0]]);\n",
    "vlist=[v1,v2,v3,v4,v5,v6,v7,v8,v9]\n",
    "wlist=[]\n",
    "for vec in vlist:\n",
    "    wlist.append(projv(vec).list())\n",
    "M=matrix(Z,wlist).transpose()\n",
    "print M.dimensions()\n",
    "print M.rank()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we compute the matrices for the adjoint action of Xmat and Ymat with respect to this basis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "adxlist=[]\n",
    "for vec in vlist:\n",
    "    a=vector((Xmat*projv(vec)*~Xmat).list())\n",
    "    b=M.solve_right(a).list()\n",
    "    adxlist.append(b)\n",
    "admatx=matrix(Z,adxlist).transpose()\n",
    "adylist=[]\n",
    "for vec in vlist:\n",
    "    a=vector((Ymat*projv(vec)*~Ymat).list())\n",
    "    b=M.solve_right(a).list()\n",
    "    adylist.append(b)\n",
    "admaty=matrix(Z,adylist).transpose()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we compute the space of cocycles. Let $r=xwy^{-1}w^{-1}$ be the relation in the presentation for $\\Gamma$. There is a map $\\mathfrak{v}\\times \\mathfrak{v}\\to \\mathfrak{v}$ given by $(a,b)\\mapsto \\frac{\\partial r}{\\partial x}\\cdot a+\\frac{\\partial r}{\\partial y}\\cdot b$, where $\\frac{\\partial r}{\\partial x}$ and $\\frac{\\partial r}{\\partial y}$ are the Fox derivatives of the relation with respect to the generators. It is well known that the space of cocylces is given by the kernel of this map. We now write a matrix, CocycleMat, for this map with respect to our basis for $\\mathfrak{v}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Defining xx, yy\n"
     ]
    }
   ],
   "source": [
    "F=FreeGroup('xx,yy')\n",
    "F.inject_variables();\n",
    "w=yy*xx*~yy*~xx*~yy*xx*yy*~xx*~yy*~xx*yy*xx\n",
    "r=xx*w*~yy*~w"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "drdx=r.fox_derivative(xx,[admatx,admaty])\n",
    "drdy=r.fox_derivative(yy,[admatx,admaty])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "CocycleMat=block_matrix(1,2,[drdx,drdy])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CocycleMat.rank()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The rank of CocycleMat is 8, and so its kernel is 10-dimensional, and so $M$ is infinitesimally rigid rel. $\\partial M$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Computing $c_a$ and $c_b$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $[z]$ be a gemerator of $H^1_{\\rho_{hyp}}(\\Gamma,\\mathfrak{v})$ then $[z]=c_a\\frac{\\partial}{\\partial a}+c_b\\frac{\\partial}{\\partial b}$. We want to calculate these coefficients. First we find $z$. Recall that cocylces are elements of the kernel of CocylceMat. By splitting and reshaping vectors in the kernel we can reconstruct the images of $x$ and $y$ under these $z$ which we denote zx and zy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "CocyleMatKernel=CocycleMat.right_kernel()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "vec1=CocyleMatKernel.basis()[0][:9]\n",
    "vec2=CocyleMatKernel.basis()[0][9:]\n",
    "zx=matrix(Z,4,4,M*vec1)\n",
    "zy=matrix(Z,4,4,M*vec2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we compute an element delta1 of $\\mathfrak{v}^{\\rho_{hyp}(x)}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "delta1=matrix(Z,[[-1,0,0,0],[0,-1,0,0],[0,0,3,0],[0,0,0,-1]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We check that delta1 is in $\\mathfrak{v}$ and commutes with Xmat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n"
     ]
    }
   ],
   "source": [
    "print Xmat*delta1-delta1*Xmat\n",
    "print projv(delta1)-delta1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we compute the Poincare pairing $d_1$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d1=4*(zx*delta1).trace();d1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let L be the homologically determined longitude that commutes with Xmat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "L=W*Xmat*Ymat*Xmati*Ymati*Xmati*Ymat*Xmat*Ymati*Xmati*Ymati*Xmat*Ymat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We check that it commutes with Xmat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0 0 0 0]\n",
       "[0 0 0 0]\n",
       "[0 0 0 0]\n",
       "[0 0 0 0]"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "L*Xmat-Xmat*L"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we compute an element delta2 of $\\mathfrak{v}^L$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "lr=L[1,3]\n",
    "li=L[2,3]\n",
    "delta2=matrix(Z,[[-1,0,0,0],[0,-(lr^2-3*li^2)/(lr^2+li^2),-4*lr*li/(lr^2+li^2),0],[0,-4*lr*li/(lr^2+li^2),(3*lr^2-li^2)/(lr^2+li^2),0],[0,0,0,-1]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We check that delta2 is in $\\mathfrak{v}$ and commutes with $L$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n",
      "[0 0 0 0]\n"
     ]
    }
   ],
   "source": [
    "print projv(delta2)-delta2\n",
    "print L*delta2-delta2*L"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we compute the image of L under z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "longit=w*xx*~yy*~xx*yy\n",
    "derlongvec=longit.fox_derivative(xx,[admatx,admaty])*vec1+longit.fox_derivative(yy,[admatx,admaty])*vec2\n",
    "zlong=matrix(Z,4,4,M*derlongvec)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we compute the Poincare pairing $d_2$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "122*z^5 + 244*z^4 + 31*z^3 - 91*z^2 - 91*z - 86"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d2=4*(zlong*delta2).trace();d2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From Lemma 5.12 there is a linear relationship between $c_a$ and $c_b$ and certain Poincare pairings $d_1$ and $d_2$.\n",
    "\n",
    "Let CoefMat be the matrix encoding thie linear relationship"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "CoefMat=-16*matrix(Z,2,2,[[1,0],[lr*(lr^2-3*li^2)/(lr^2+li^2),li*(li^2-3*lr^2)/(lr^2+li^2)]]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now recover $c_a$ and $c_b$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "(-33/16*z^5 - 5/4*z^4 - 51/32*z^3 + 33/32*z^2 + 55/32*z + 23/16)*i\n"
     ]
    }
   ],
   "source": [
    "(ca,cb)=CoefMat.solve_right(vector(Z,[d1,d2]))\n",
    "print ca\n",
    "print cb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$c_b$ is nonzero and so the map $\\mathsf{res}_\\ast:H^1_{\\rho_{hyp}}(\\Gamma,\\mathfrak{v})\\to H^1_{\\rho_{hyp}}(\\langle l\\rangle,\\mathfrak{v})$ is non-trivial"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 7.4",
   "language": "",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
