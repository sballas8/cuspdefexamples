### Cusp Deformation Computations

This repository contains computations descirbed in the paper "Constructing convex projective 3-manifolds with generalized cusps" (https://arxiv.org/abs/1805.09274)

There are two files. Each is an ipython notebook that can be executed in sage. 

### 5_2 Example

The first file is 5_2rigid.ipynb and contains computations to that supplement the proof that the complement of the 5_2 knot admits a family of convex projective structues with a type 2 cusps. 

### 6_3 Example

The first file is 6_3rigid.ipynb and contains computations to that supplement the proof that the complement of the 6_3 knot admits a family of convex projective structues with a type 1 cusp. 

